@extends('layouts.master')
@section('content')
        <div class="page-header">
            <h3>Восстановление пароля</h3>
        </div>
    <div class="col-lg-5">
        <form method="POST" action="{{ URL::to('/users/forgot_password') }}" accept-charset="UTF-8">
            <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">

            <div class="form-group">
                <label for="email">Введите свой E-mail</label>
                <span class="help-block">На него будут отправлены инструкции по восстановлению Вашего пароля</span>

                <div class="input-append input-group">
                    <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text"
                           name="email" id="email" value="{{{ Input::old('email') }}}">
            <span class="input-group-btn">
                <input class="btn btn-primary" type="submit"
                       value="{{{ Lang::get('confide::confide.forgot.submit') }}}">
            </span>
                </div>
            </div>


            @if (Session::get('error'))
                <div class="alert alert-error alert-danger">{{{ Session::get('error') }}}</div>
            @endif

            @if (Session::get('notice'))
                <div class="alert">{{{ Session::get('notice') }}}</div>
            @endif
        </form>
    </div>
@stop